import { Injectable } from '@angular/core';
import {ROIModel} from '../roimodel';
import {PieDataClass} from '../pie-chart-data';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';
const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root',
})

export class NodeDataService {
  private url ='/nodeapi';
  localList :ROIModel[]=[];

  constructor(private http: HttpClient) { }

  apply(roimodel:ROIModel):Observable<any>{
    // this.localList.push(roimodel);
    var saveUrl = this.url+"/ROI";
    return this.http.post(saveUrl,roimodel,httpOptions);
  }

  mailValues(mailId:string):Observable<any>{
    var mailurl = this.url+"/sendMail?mailId="+mailId;
    // return this.http.post(this.url, this.localList, httpOptions);
    return this.http.post(mailurl, httpOptions);
  }
}