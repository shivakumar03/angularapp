import { Component, OnInit, OnChanges } from '@angular/core';
import {ROIModel} from './roimodel';
import {PieDataClass} from './pie-chart-data';
import { NodeDataService } from './service/node-data.service';
import {MatSnackBar} from '@angular/material';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'ROI App';

  roiModel:ROIModel={
    BuyAmount : 10,
    BuyStage:30,
    HoldPeriod :24,
    TotalValue:58500,
    ReturnsObtained:10114,
    TotalAssets:68614,//sample values
  }
  inputArr:number[] = [0, 2.5, 5,10,15,20,25,30,50];  
  sliderBuyStage = 8;
  inputBuyStage = 30;
  MailId:string="";
  message:string="";

  pieData:PieDataClass[]=[];

  constructor (private dataService: NodeDataService,public snackBar: MatSnackBar) {   

  }
  ngOnInit(){
    this.CalculateROI();
  }

  BindBuyStageInput():void{
    this.inputBuyStage = this.inputArr[this.sliderBuyStage];
    this.roiModel.BuyStage = this.inputBuyStage;
    this.CalculateROI();
  }

  BindBuyStageSlider():void{//bind slider on change of input
    var index = this.closestArrIndex(this.inputArr,this.inputBuyStage);
    this.sliderBuyStage = index;
    this.inputBuyStage = this.inputArr[index];
    this.roiModel.BuyStage = this.inputBuyStage;
    this.CalculateROI();
  }

  closestArrIndex(arr:number[], target:number) {
    for (var i=1; i<arr.length; i++) {
        if (arr[i] > target) {
            var p = arr[i-1];
            var c = arr[i];
            return Math.abs( p-target ) < Math.abs( c-target ) ? i-1 : i;
        }
    }
    return arr.length-1;
  }

  CalculateROI():void {
    var totalVal = Math.round((4500*this.roiModel.BuyAmount)+((this.roiModel.BuyStage/100)*(4500*this.roiModel.BuyAmount)));
    var returns = Math.round((totalVal) * (Math.pow((1+(8/1200)),this.roiModel.HoldPeriod)-1));
    this.roiModel.TotalValue = totalVal;
    this.roiModel.ReturnsObtained = returns;
    this.roiModel.TotalAssets = (totalVal + returns);
    var totalValuePerc = Math.round((totalVal/this.roiModel.TotalAssets)*100);
    var returnPerc = 100 - totalValuePerc;    
    this.pieData = [
      {label:totalValuePerc.toString()+"%",value:totalValuePerc},
      {label:returnPerc.toString()+"%",value:returnPerc},
    ];
  }

  apply():void{
    this.dataService.apply(this.roiModel).subscribe(
      (data:any)=>{console.log(data);this.message="Saved Successfully";this.OpenSnackBar();},
      error => {this.message="Something wrong. Please try again.";this.OpenSnackBar();}
    )
  }

  success:boolean=true;
  mail(mailId:string):void{
    this.dataService.mailValues(this.MailId).subscribe( (data:any)=>{
      // console.log("s");
      this.success=  true;
      this.message = "Mailed Successfully";
      this.OpenSnackBar();         
    },
    error=>{
      // console.log("n");
      this.success =  false;
      this.message = "Something went wrong on server. Please try again.";
      this.OpenSnackBar();   
    });
  }

  OpenSnackBar(){
    if(this.message){
      this.snackBar.open(this.message,"Done",{
        duration: 2000,
      });
    }
  }
  
  ngOnChanges(){
  }

}
