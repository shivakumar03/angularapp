export class ROIModel {
    BuyAmount: number;
    BuyStage: number;
    HoldPeriod: number;
    TotalValue:number;
    ReturnsObtained:number;
    TotalAssets:number;
}